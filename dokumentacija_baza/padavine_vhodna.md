#### PADAVINE_VHODNA

V tabeli padavine_vhodna so vpisani podatki iz padavinskih postaj (tip=1). Podatke napisane v padavinski dnevnik za pretekli mesece opazovalci iz padavinskih postaj po pošti pošljejo v Ljubljano od koder jih nato pošljem za vnos v Bilje. Praviloma je večina podatkov vnešenih do konca drugega tedna v mesecu, vendar pa nekateri opazovalci všasih podatkov ne pošljejo pravočasno. Tako se lahko zgodi, da so lahko podatki nekaterih postaje zaradi zamude s strani opazovalcev vnešene tudi več tedno kasneje glede na ostale postaj. Podatke se nato v postopku kontrole v najkrajšem možnem času skontrolira.

Več o tem je bilo napisano [tukaj](http://venera.arso.sigov.si/meteo/Kontrola/dokument/kontrola.html).

Struktura tabele:

| Column                        |             Type            |                          Modifiers                          |
|-------------------------------|:---------------------------:|:-----------------------------------------------------------:|
| postaja                       |           integer           |                                                             |
| leto                          |           smallint          |                                                             |
| mesec                         |           smallint          |                                                             |
| dan                           |           smallint          |                                                             |
| p5_padavine                   |           smallint          |                                                             |
| p6_oblika                     |           smallint          |                                                             |
| p7_sneg_skupaj                |           smallint          |                                                             |
| p8_sneg_novi                  |           smallint          |                                                             |
| p9_dez_sneg_dezsneg           |           smallint          |                                                             |
| p10_toca_slana_megla          |           smallint          |                                                             |
| p11_ivje_sodra_bpseno         |           smallint          |                                                             |
| p12_poledica_nevihta_vihveter |           smallint          |                                                             |
| p13_sneznaodeja_rosa          |           smallint          |                                                             |
| p14_interpolacija             |           smallint          |                                                             |
| idmm                          |           integer           |                           not null                          |
| datum                         |             date            |                           not null                          |
| cas_vnosa                     | timestamp without time zone | not null default ('now'::text)::timestamp(6) with time zone |
| ime_vnasalca                  |      character varying      |                                                             |
Indexes:
    "padavine_vhodna_idx" btree (idmm, datum)
    "padavine_vhodna_postaja_index" btree (postaja)
    "postaja_lmd_datum_idmm_ppd_index" btree (postaja, leto, mesec, dan, datum, idmm)

