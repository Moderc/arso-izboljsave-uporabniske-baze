#### AMP_V

V tabelo so vpisani podatki na

Struktura tabele je:

  Column   |            Type             | Modifiers
| :-----:|:-------------:| :-----:|
 idmm            | smallint                    |
 tip             | smallint                    |
 datum           | date                        |
 cas             | time without time zone      |
 par             | smallint                    |
 vred            | real                        |
 svred           | real                        |
 vvpov           | real                        |
 svpov           | real                        |
 vvmax           | real                        |
 svmax           | real                        |
 tmax            | time without time zone      |
 vvmin           | real                        |
 svmin           | real                        |
 tmin            | time without time zone      |
 stdev           | real                        |
 pov             | real                        |
 status          | smallint                    |
 statusi         | real[]                      |
 stdev_x         | real                        |
 stdev_y         | real                        |
 max_hitrost     | real                        |
 t_max_hitrost   | time without time zone      |
 min_hitrost     | real                        |
 t_min_hitrost   | time without time zone      |
 stabilnost      | real                        |
 datum_cas       | timestamp without time zone |
 status_senzorja | text                        |

Indexes:
    "amp_v_idx" UNIQUE, btree (idmm, tip, datum, cas, par, datum_cas)
    "amp_v_datum_cas" btree (datum_cas)

Stolpec statusi je vektorsko zapisan. Za meritve na starih postajah je na 1. mestu zapisan stat_k, na 2. mestu pa status kolomna. Gre za prvostopenjsko kontrolo, ko se preveri logične meje. Pri bobrovih postajah je na 4. mestu (statusi[4]) zapisana [veljavnost podatka](http://tmpvirga.rzs-hm.si/~mihad/kontrola_bobrovih_indeksov/opis_kontrol.pdf), ki ga beleži sama postaja. Na 7. mestu (statusi[7]) je zapisan delež pravilno izmerjenih podatkov. Za analizo veljavnosti podatkov klikni [tukaj](http://tmpvirga.rzs-hm.si/~mihad/kontrola_bobrovih_indeksov/analiza_statusov.php).

Parametri, ki se nahajajo v tabeli amp_p so naslednji:

 |parameter| opis|
|:-------------:| :-----|
6	|	veter, procesirane vrednosti hitrosti in smeri vetra 10 m nad tlemi iz primarnega anemometra [m/s , o] ali ročni anemometer na klasični p.
23	|	veter na 40m stolp NEK
24	|	veter na 70m stolp NEK
78	|	redundančna meritev vetra
3180	|	veter, procesirane vrednosti hitrosti in smeri vetra 10 m nad tlemi iz primarnega anemometra [m/s , stopinje].
3181	|	
3188	|	veter na letališčih na pragu 31,procesirane vrednosti hitrosti in smeri vetra 10 m nad tlemi iz redundantnega anemometra na letališčih [m/s , °] RWY / TDZ
3189	|	veter na letališčih na pragu 13,procesirane vrednosti hitrosti in smeri vetra 10 m nad tlemi iz anemometra na letališčih [m/s , °] RWY / END

Za prepis podatkov iz Tajfuna (tabele podatki_veter in podatki_k_2) na tmpvirgo v tabelo amp_v skrbi več različnih podatkov, ki se nahajajo na naslednjih naslovih:
* /home/mihad/programi/oracle/amp_v/crpanje_tabela_podatki_veter.pl , časi prepisa so vsako uro ob 6,12,36,42 minuti
* /home/mihad/programi/oracle_10min/amp_v/prenos_amp_v_10min.pl, časi prepisa so vsako uro ob 8,11,38,41 minuti
