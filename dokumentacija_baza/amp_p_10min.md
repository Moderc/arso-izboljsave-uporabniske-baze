#### OPIS TABELE AMP_P_10MIN

V tabelo so vpisani 10 minutni podatki iz bobrovih postaj. Podatki so prepisani iz tabele podatki_k1, ki se nahajo na Tajfunu. 

Struktura tabele je:

|  Column   |            Type             | opis stolpca
|-----|:-------------:|:-----|
| idmm            | smallint                    |
| tip             | smallint                    |
| par             | smallint                    |
| datum           | date                        |
| cas             | time without time zone      |
| r1              | real                        |1. pet minut
| r2              | real                        |2. pet minut
| r3              | real                        |vsota ali povprečje
| r4              | real                        |
| statusi         | real[]                      |
| cas1            | time without time zone      |
| cas2            | time without time zone      |
| datum_cas       | timestamp without time zone |
| status_senzorja | text                        |

Indexes:
    "amp_p_10min_idx" UNIQUE, btree (idmm, tip, datum, cas, par, datum_cas)
    "amp_p_10min_cas" btree (cas)
    "amp_p_10min_datum" btree (datum)
    "amp_p_10min_idmm" btree (idmm)

Pri bobrovih postajah je na 4. mestu (statusi[4]) zapisana [veljavnost podatka](http://tmpvirga.rzs-hm.si/~mihad/kontrola_bobrovih_indeksov/opis_kontrol.pdf), ki ga beleži sama postaja. Na 7. mestu (statusi[7]) je zapisan delež pravilno izmerjenih podatkov. Za analizo veljavnosti podatkov klikni [tukaj](http://tmpvirga.rzs-hm.si/~mihad/kontrola_bobrovih_indeksov/analiza_statusov.php).

Parametri, ki se nahajajo v tabeli amp_p_10min so naslednji:

 |parameter| opis|
|:-------------:| :-----|
|3120	|	padavine - količina padavin [mm],|tehtalni ombrometer Pluvio2
|3121	|	napolnjenost zbirne posode [mm in |odstotki] bucket NRT
|3124	|	optično izmerjena intenziteta padavin |[mm/h]
|3125	|	optično izmerjena količina padavin [mm]
|3126	|	sedanje vreme SYNOP (WMO 4680)
|3127	|	simulacija radarske odbojnosti v |prizemni plasti [dBZ]
|3129	|	trajanje padavin [min]
|3132	|	
|3150	|	skupna višina snežne odeje [cm]
|3151	|	višina snega-jakost signala (signal |strength)
|3304	|	trajanje sončnega obsevanja v desetinah |ure ali [min]
|3420	|	vlažnost lista (0 ali 1),trajanje omočenosti listja [min

Za prepis podatkov iz Tajfuna (tabele podatki_k_1) na tmpvirgo v tabelo amp_p_10min skrbi program:
* /home/mihad/programi/oracle_10min/amp_p/prepis_v_amp_p_10min.pl , časi prepisa so vsako uro ob 5,19,35,49 minuti
