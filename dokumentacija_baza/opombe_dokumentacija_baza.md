# Pregled dokumentacije

Ob pregledu dokumentacije sem zabeležila, da kateri linki na določenih straneh ne delujejo.
Navedla sem naslov literature, link do nje ter nedelujoč link dotični strani.

### 1.) Baza meteoroloških podatkov
(http://tmpvirga.rzs-hm.si/~mihad/dokumentacija_baza/baza_meteoroloskih_podatkov.htm)

##### 3 Organizacija meteorološke baze podatkov

Ne delujeta linka do slik:
1. http://tmpvirga.rzs-hm.si/~mihad/dokumentacija_baza/Datoteke_za_Baza_meteoroloskih_podatkov/image001.gif

2. http://tmpvirga.rzs-hm.si/~mihad/dokumentacija_baza/Datoteke_za_Baza_meteoroloskih_podatkov/shema_tabel_meteo02.png

##### 4.2 Index verodostojnosti

Ne deluje link do slike:
1. http://tmpvirga.rzs-hm.si/~mihad/dokumentacija_baza/Datoteke_za_Baza_meteoroloskih_podatkov/image002.jpg

### 2.) Dokument: parametri_stev_pretvorbe.html
(http://venera.arso.sigov.si/Meteo/Kontrola/dokument/parametri_stev_pretvorbe.html)

potrebno urediti izgled tabel


### 3.) Dokument: kontrola klasičnih podatkov
(http://venera.arso.sigov.si/meteo/Kontrola/dokument/kli_kon.html)

Ne dela povezava do strani: Ekstremi in osnovna statistika za klimatološke met. postaje [ta]
(http://venera.arso.sigov.si/Kontrola/sta_kli_od1961)

### 3.) Dokument: kontrola klasičnih podatkov
(http://venera.arso.sigov.si/meteo/Kontrola/dokument/kli_kon.html)

Ne dela povezava do strani:
 - http://www.geog.uu.nl/gstat/
 - http://www.geog.uu.nl/gstat/manual/gstat.html
 - .ftp://ftpmaths.anu.edu.au/pub/meschach/meschach.html 
 - .ftp://cmpc1.phys.soton.ac.uk/ 

### Vprašanja pri bazi AMEBA:

1.) Relacija med tabelami:
user:student
pass:student
(Z) - zaklenjene tabele
(P) - prazne tabele
(D) - duplicirane/podvojene tabele 
() - ni posebnosti

Pregled tabel v bazi:
 - adcon_vhodna (P) 
 
 SKLOP datum:
 - amp_datum (datum do 2020-12-31?)
 - amp_datum_bober (datum do 2030-12-31?)
 - amp_datum_bober_x (Z)

SKLOP: 
 - amp_klima ()

 SKLOP amp_o:
 - amp_o (en del pol pri vnosu je prazen - mogoče je povezano s poljem #tip)
 - amp_o_popravek
 - amp_o_test (P)
 - amp_o_test_test (P)
 - amp_o_virga (P)

SKLOP amp_obl:
 - amp_obl
 - amp_obl (zadnji podatki za leto 2011)
 - amp_obl_test (zadnji podatki za leto 2011)
 - amp_obl_virga (P)

SKLOP amp_op:
 - amp_op
 - amp_op_popravek
 - amp_op_test (zadnji podatki za leto 2011)
 - amp_op_virga (P)

SKLOP amp_p:
 - amp_p
 - amp_p0 (zadnji podatki za leto 2011)
 - amp_p_10min
 - amp_p_popravek
 - amp_p_virga (P)

SKLOP amp_r:
 - amp_r
 - amp_r_test (zadnji podatki za 2013)

 SKLOP amp_sv:
 - amp_sv
 - amp_sv_popravek (P)
 - amp_sv_test (zadnji podatki za leto 2011)

 SKLOP amp_v:
  - amp_v 
  - amp_v_kopija_14dec2011 (Z)
  - amp_v_kopija_29nov2013 (Z)
  - amp_v_kopija_hipe (datum do 2019?)
  - amp_v_popravek (Z)
  - amp_v_razlike_s_kopija_14dec2011 (Z)
  - amp_v_razlike_s_kopija_29nov2013 (Z)
  - amp_v_test (zadnji podatki za 2014)
  - amp_v_virga_kopija_6dec2013 (Z)

SKLOP amp_vl:
 - amp_vl (zadnji podatki za leto 2015)
 - amp_vl_popravek (samo 8 vrstic za leto 2011)
 - amp_vl_test (zadnji podatki za leto 2011)

SKLOP:
 - biovremenska_napoved
 - bogo_t71f_s2f (Z)
 - dan_ure (Z)
 - datum (datum do 2019?)
 - datum_klasika (datum do 2049?)
 - davis (Z)
 - db_output (Z)

 SKLOP depese:
 - depese_dekodirane
 - depese_dekodirane_prejsnji_vnosi 
 - depese_dekodirane_staro (zadnji podatki za 2013)
 
 SKLOP:
 - django_migrations (Z)
 
 SKLOP dnevna:
 - dnevna_agro (Z)
 - dnevna_inter_padavine
 - dnevna_padavine
 - dnevna_padavine_test (zadnji podatki za leto 2013)
 - dnevna_par
 - dnevna_par_pomozna
 - dnevna_par_pomozna_test
 - dnevna_par_test
 - dnevna_pojavi_1
 - dnevna_pojavi_2
 - dnevna_sevanje
 - dnevna_sevanje_test
 - dneva_temperatura-od21do21 (zadnji podatki za leto 2014)
 - dnevna_temperaturni_primanjkljaj_presezek

SKLOP dogodki:
 - dogodki_na_postajah (zadnji podatki za leto 2010)
 - dogodki_na_postajah_ismm (Z)
 - dogodki_na_postajah_test (Z)

SKLOP etp:
 - etp (zadnji podatki za leto 2016)
 - etp2008 (Z)
 - etp_amp_par (Z)
 - etp_amp_tt (Z)
 - etp_kli_par (Z)
 - etp_model
 - etp_sproti
 - etp_sproti_interp
 - etp_tmp (Z)
 - etp_tmp_postaje (Z)

SKLOP:
 - evidencapos (Z)
 - fenoloska_vhodna (P)
 - fotografije (zadnji podatki za leto 2006)
 - geografske_inf (zadnji podatki za leto 2006)
 - geometry_columns (Z)
 - glavna_podrocja (Z)
 - glavna_podrocja_x (Z)
 - gumbki (zadnji podatki za leto 2015)
 - gumbki_tem_vla

SKLOP id:
 - id_obs (datum_zaceka z 2029?)
 - id_obs_test (datum zacetka z 10100?)
 - id_obs_x (datum konca 10000?)
 - id_red (Z)

SKLOP ident:
 - ident_pos_staro (D?)
 - ident_posx (D?)
 - ident_posx_test (D?)

SKLOP idmm:
 - idmm (D?)
 - idmm_geom_tmp
 - idmm_old (D?)

SKLOP kli:
 - kli_test (Z)
 - kli103 (Z)

SKLOP klima:
 - klima_vhodna
 - klima_popravi (Z)
 - klima_pomozna
 - klima_ekstremi

SKLOP:
 - komentarji
 - kontrola_rh_p_t_interpolacija
 - koordinate_radar
 - kratice
 - lavinski_podatki (zadnji podatki za leto 2013)
 - mesecna

SKLOP:
 - modelska_napoved (zadnji podatki za 2014 - owner: mihad)
 - msonce (Z)
 - na_danasnji_dan (zadnji podatki za leto 2011 - owner: base)
 - nek (Z)

SKLOP ombrografi
 - ombrografi_vhodna_staro (zadnji podatki za 2007 - owner: postgres)
 - ora_idmm (Z)

SKLOP opazovanja
 - oznake_opazovanj_staro (zadnji podatki za 2005 - owner: postgres)

SKLOP padavine:
 - pad_p (Z)
 - padavine_5min 
 - padavine_korigirane (zadnji podatki za 2016 - owner bertalanic)
 - padavine_vhodna

SKLOP parametri:
 - parametri
 - parametri_opis
 - parametri_opis_old (ali se še uporablja - owner: postgres)
 - parametri_x (ali se še uporablja - owner: base)

SKLOP pga: (zakaj so te tabele prazne? owner: postgres)
 - pga_diagrams (P)
 - pga_forms (P)
 - pga_graphs (P)
 - pga_images (P)
 - pga_layout (1 vrstica)
 - pga_queries (P)
 - pga_reports (P)
 - pga_scripts (P)

SKLOP pgadmin (zakaj so te tabele prazne? owner: base):
 - pgadmin_desc (P)
 - pgadmin_param (P)
 - pgadmin_rev_log (P)
 - pgadmin_seq_cache (P)
 - pgadmin_table_cache (P)

SKLOP:
 - pl5_vhodna

SKLOP pluviografi:
 - pluviografi_interpolacije
 - pluviografi_vhodna
 - pluviografi_vhodna_stara (zadnji podatki za leto 2014. owner: base)

SKLOP:
 - pmsv (Z)

SKLOP podatkovne_tabele:
 - podatkovne_tabele (D?)
 - podatkovne_tabele_x (D?)

 SKLOP:
  - podpodrocja

SKLOP polurna:
 - polurna_di_uv_dv
 - polurna_di_uv_dv_testn (zadnji podatki za leto 2014. owner: gvert)
 - polurna_globalno
 - polurna_globalno_test (zadnji podatki za leto 2014. owner: gvert)
 - polurna_padavine
 - polurna_padavine_test (zadnji podatki za leto 2013. owner: base)
 - polurna_ptr
 - polurna_ptr_test (ali se ta tabela kje uporablja? owner: mihad)
 - polurna_veter 
 - polurne_inter_padavine
 - polurne_inter_padavine_test (ali se ta tabela kje uporablja? owner: mihad)

 SKLOP:
 - post_homogen (Z)
 - postaja (Z) 
 - povp7100_trs (Z)
 - povp8110_trs (Z)
 - ppd_org (vnosi za kleto 2047? owner: postgres)
 - ppd_rezerva (vnosi za kleto 2489? owner: base)
 - ppd_vhodna
 - prijatelji_bivanje (ali to tbelo kje potrebujemo ? owner: base)
 - prispelost_klima (Z)
 - pritisk_urni_vhodna (zadnji podatki za leto 1970- owner: postgre)
 - regije_agro
 - relvlaga_urni_vhodna
 - sgu_ascii (ali se ta tabela kje potrebuje? owner: base)
 - slana_tmp (Z - ali se kje potrevbuje? owner: habic)
 - sliva_cveti (Z - ali se kje potrevbuje? owner: habic)
 - son_int_koef (ali se ta tabela potrebuje? owner: postgres)
 - sonce_glo_rfl_vhodna (P)
 - sonce_tra_rfl_vhodna (P)
 - spatial_ref_sys
 - spi (ali se ta tabela potrebuje (podatki za 2012)? owner: student )
 - spi1 (Z. owner: grega)
 - spletne_kamere

SKLOP METAPODATKI:
 - spremembe_metapodatki
 - spremembe_metapodatki1 (zadnji podatki za leto 2010. owner: mihad)

SKLOP STABILNOST:
 - stabilnost (Z)
 - stabilnost_kopija_6januar2016 (ali je ta tabela potrebna? owner: bertalanic)
 - stabilnost_test (ali je ta tabela potrebna? owner: bertalanic)

SKLOP:
 - stu_vhodna
 - t2m_letos (Z. ali je ta tabela še potrebna v bazi? owner: habic)
 - tef71_00 (zadni vnosi za leto 200. owner: habic)
 - temmorja_vhodna (zadnji vnosi za leto 2016. owner: postgres)

SKLOP TEMPERATURA:
 - temp_5minutne_padavine (Z. owner: bertalanic)
 - temp_5minutne_padavine_pluvio (Z. owner: bertalanic)
 - temp_homogen (Z. owner: habic)
 - temperatura_rfl_vhodna (P)
 - temperatura_urni_vhodna
 - temperatura_urni_vhodna_old (zadnji vnosi za leto 2007. jo potrebujemo? owner:postgres)

SKLOP temtal:
 - temtal_interpolacije
 - temtal_vhodna
 - temtal_vhodna_2 (Z. owner: base)
 - temtal_vhoda_test (zadnji podatki za leto 2013. owner: base)

SKLOP:
 - ter_par_test (zadnji podatki za leto 2007. owner: base)
 - termini_agro (Z. owner: habic)
 - terminska_par
 - terminska_par_test (se potrebuje? owner: base)
 - terminska_pojavi
 - terminska_reduciran_p
 - termistat_agro (Z. owner: habic)
 - tes_tet_amp (P. owner: base)
 - tes_tet_ampi (zadnji vnosi za leto 2011. owner: base)
 - tes_tet_ampx (zadnji vnosi za leto 2010. owner: base)
 - test_id_obs (datum konca 1000-01-01? owner: base)
 - test_idmm  (Z. owner: mihad)
 
 SKLOP tipi_grupe:
 - tipi_grupe (D?)
 - tipi_grupe_seq_x (1 vrstica? owner: base)
 - tipi_grupe_x (D?)

 SKLOP tipi_postaj:
  - tipi_postaj (D?)
  - tipi_postaj_seq_x (1 vrstica? owner: base)
  - tipi_postaj_x (D?)

SKLOP:
 - totalizatorji_vhodna (zadnji vnosi za leto 2011. owner: base)
 - trajanje_vhodna
 - ts6190 (Z. owner: habic)
 - ts8110 (Z. owner: habic)
 - ttest (1 vrstica? owner: base)
 - tuja_klima (zadnji vnosi za leto 2015. owner: base)
 - tuja_klima_test (Z. owner: base)
 - vegetacija
 
SKLOP variable:
- variable (D?)
- variable_x (D?)

SKLOP veter:
 - veter
 - veter_10min_ames_vhodna (P. owner: base)
 - veter_10min_vhodna (P. owner: base) 
 - veter_dnevni_sunki_mesec (Z. owner: bertalanic)
 - veter_kopija (Zadnji vnosi za leto 2011. owner: bertalanic)
 - veter_kratice (owner: bertalanic)
 - veter_max_vhodna (P. owner: postgres)
 - veter_mesec (zadnji vnosi za leto 2016. owner: bertalanic)
 - veter_moc_mesec (Z. owner: bertalanic)
 - veter_moc_mesec_test (Z. owner: bertalanic)
 - veter_napake (Z. owner: bertalanic)
 - veter_rfl_vhodna (P. owner: postgres)
 - veter_sinopticni32_vhodna (P. owner: postgres)
 - veter_sinopticni36_vhodna (P. owner: postgres)
 - veter_test (Z. owner: bertalanic)
 - veter_test_napake (Z. owner: bertalanic)
 - veter_u_meh_anemograf32_vhodna (zadnji vnosi za 2008. owner: base )
 - veter_u_meh_anemograf36_vhodna (P. owner: postgres )
 - veter_uporab (zadnji vnosi za leto 2016. owner: bertalanic)
 - veter_uporab_napake (Z. owner: postgres)
 - veter_urni_ames_vhodna (P)
 - veter_urni_anemograf32_vhodna (P. owner:postgres)
 - veter_urni_anemograf36_vhodna (P. owner:postgres)
 - veter_visina_meritev (Z. owner: bertalanic) 
 - veter_vku_vhodna (P)
 - veter_vrr_vhodna (P)

 SKLOP:
  - vlaga_rfl_vhodna(P)
  - vremenska napoved
  - vsefeno (Z. owner: habic)



























